// Fill out your copyright notice in the Description page of Project Settings.

#include "twinstickshootercpp.h"
#include "Weapon.h"
#include "Projectile.h"

// Sets default values
AWeapon::AWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent * DefaultSceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));
	this->SetRootComponent(DefaultSceneRoot);

	this->GunMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("GunMesh"));
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> GunMeshAsset(TEXT("SkeletalMesh'/Game/FPWeapon/Mesh/SK_FPGun.SK_FPGun'"));
	this->GunMesh->SetSkeletalMesh(GunMeshAsset.Object);
	this->GunMesh->AttachTo(GetRootComponent());
	this->GunMesh->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	this->ProjectileSpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("ProjectileSpawnPoint"));
	this->ProjectileSpawnPoint->AttachTo(this->GunMesh);
	this->ProjectileSpawnPoint->SetRelativeLocation(FVector(0.0f, 53.0f, 11.0f));
	this->ProjectileSpawnPoint->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));

	this->DoOnceFlag = true;
	this->RoundsPerSecond = 5;
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeapon::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AWeapon::PullTrigger()
{
	if (DoOnceFlag) {
		this->Fire();
		GetWorldTimerManager().SetTimer(TriggerTimerHandler, this, &AWeapon::Fire, 1.0f / this->RoundsPerSecond, true);
		DoOnceFlag = false;
	}
}

void AWeapon::ReleaseTrigger()
{
	GetWorldTimerManager().ClearTimer(TriggerTimerHandler);
	DoOnceFlag = true;
}

void AWeapon::Fire()
{
	UWorld* const World = GetWorld();
	if (World) {
		FTransform SpawnPoint = this->ProjectileSpawnPoint->GetComponentTransform();
		AActor * NewProjectile = World->SpawnActor <AProjectile>(SpawnPoint.GetLocation(), SpawnPoint.GetRotation().Rotator());
	}
}