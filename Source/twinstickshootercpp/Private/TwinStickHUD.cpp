// Fill out your copyright notice in the Description page of Project Settings.

#include "twinstickshootercpp.h"
#include "TwinStickHUD.h"
#include "BaseCharacter.h"
#include "twinstickshootercppGameMode.h"

float UTwinStickHUD::HealthBar()
{
	ABaseCharacter * BaseCharacter = Cast<ABaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	return BaseCharacter->Health / 100.0f;
}


int32 UTwinStickHUD::Score()
{
	AtwinstickshootercppGameMode * GameMode = Cast<AtwinstickshootercppGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	return GameMode->Score;
}
