// Fill out your copyright notice in the Description page of Project Settings.

#include "twinstickshootercpp.h"
#include "EnemyAI.h"

void AEnemyAI::BeginPlay()
{
	FTimerHandle handler;
	GetWorldTimerManager().SetTimer(handler, this, &AEnemyAI::TrackPlayer, 1.0f, true, 0.0f);
}

void AEnemyAI::TrackPlayer()
{
	APlayerController * PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	
	ACharacter * PlayerCharacter = PlayerController->GetCharacter();
	if (PlayerCharacter) {
		this->MoveToActor(PlayerController->GetCharacter(), 5);
	}
}

