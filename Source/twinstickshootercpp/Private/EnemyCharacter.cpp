// Fill out your copyright notice in the Description page of Project Settings.

#include "twinstickshootercpp.h"
#include "EnemyCharacter.h"
#include "EnemyAI.h"

AEnemyCharacter::AEnemyCharacter() :ABaseCharacter(), EnemyColor(0.96f, 0.128f, 0.053f) {
	//Set the hero mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> EnemyMeshAsset(TEXT("SkeletalMesh'/Game/AnimationAssets/UE4_Mannequin/Mesh/SK_Mannequin.SK_Mannequin'"));
	this->GetMesh()->SetSkeletalMesh(EnemyMeshAsset.Object);

	static ConstructorHelpers::FObjectFinder<UAnimBlueprintGeneratedClass> EnemyAnimBPAsset(TEXT("AnimBlueprintGeneratedClass'/Game/AnimationAssets/EnemyAnimBP.EnemyAnimBP_C'"));
	this->GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	this->GetMesh()->AnimBlueprintGeneratedClass = EnemyAnimBPAsset.Object;

	/*
	static ConstructorHelpers::FObjectFinder<UAnimBlueprint> EnemyAnimBPAsset(TEXT("AnimBlueprint'/Game/AnimationAssets/EnemyAnimBP.EnemyAnimBP'"));
	this->GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	this->GetMesh()->AnimBlueprintGeneratedClass = EnemyAnimBPAsset.Object->GetAnimBlueprintGeneratedClass();
	*/

	//Set hero position
	this->GetMesh()->SetRelativeLocation(FVector(0.0f, 0.0f, -90.0f));
	this->GetMesh()->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	this->DamageVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("DamageVolume"));
	this->DamageVolume->InitBoxExtent(FVector(32.0f, 32.0f, 32.0f));
	this->DamageVolume->AttachTo(GetRootComponent());
	this->DamageVolume->SetRelativeLocation(FVector(58.0f, 0.0f, -65.0f));
	this->DamageVolume->SetRelativeScale3D(FVector(1.0f, 1.0f, 0.23f));
	this->DamageVolume->OnComponentBeginOverlap.AddDynamic(this, &AEnemyCharacter::OnBeginOverlap);
	this->DamageVolume->OnComponentEndOverlap.AddDynamic(this, &AEnemyCharacter::OnEndOverlap);


	this->GetCharacterMovement()->MaxWalkSpeed = 200.0f;

	this->AIControllerClass = AEnemyAI::StaticClass();

	this->Damage = -20;
	this->AttacksPerSecond = 2;
	this->Score = 500;
}

void AEnemyCharacter::OnConstruction(const FTransform& Transform)
{	
	Super::OnConstruction(Transform);
	this->DynaMat = this->GetMesh()->CreateDynamicMaterialInstance(0);
	this->DynaMat->SetVectorParameterValue("BodyColor", this->EnemyColor);
}

void AEnemyCharacter::AffectHealth_Implementation(float Delta)
{
	
	this->CalculateHealth(Delta);
	if (this->isDead) {
		//this->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		this->GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Ignore);
		this->GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);

		AtwinstickshootercppGameMode * GameMode = Cast<AtwinstickshootercppGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		GameMode->IncrementScore(Score);

		this->DetachFromControllerPendingDestroy();

		if (!GetWorldTimerManager().IsTimerActive(DeadAnimationTimerHandler)) {
			GetWorldTimerManager().SetTimer(DeadAnimationTimerHandler, this, &AEnemyCharacter::DestroyEnemy, 3.0f, false);
		}
	}

	//UE_LOG(LogTemp, Warning, TEXT("TAKING DAMAGE %f"), this->Health);
}

void AEnemyCharacter::OnBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AHeroCharacter * HeroActor = Cast<AHeroCharacter>(OtherActor);
	if (HeroActor) {
		this->Hero = HeroActor;
		GetWorldTimerManager().SetTimer(this->DamageTimerHandler, this, &AEnemyCharacter::DamageTheHero, 1.0f / this->AttacksPerSecond, true);
	}
}

void AEnemyCharacter::OnEndOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (this->Hero == OtherActor) {
		GetWorldTimerManager().ClearTimer(this->DamageTimerHandler);
		this->Hero = nullptr;
	}
}

void AEnemyCharacter::DamageTheHero()
{
	if (this->Hero) {
		IiDamageable * damageable = Cast<IiDamageable>(this->Hero);
		if (damageable) {
			IiDamageable::Execute_AffectHealth(damageable->_getUObject(), this->Damage); //This way works for c++ and blueprints
		}
	}
}

void AEnemyCharacter::DestroyEnemy()
{
	Destroy();
}