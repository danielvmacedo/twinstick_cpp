// Fill out your copyright notice in the Description page of Project Settings.

#include "twinstickshootercpp.h"
#include "BaseCharacter.h"


// Sets default values
ABaseCharacter::ABaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

//Calculate health function(helper)
void ABaseCharacter::CalculateDead()
{
	if (this->Health <= 0) 
	{
		isDead = true;
	}
	else {
		isDead = false;
	}
}

//Calculate health
void ABaseCharacter::CalculateHealth(float Delta)
{
	this->Health += Delta;
	this->CalculateDead();
}

#if WITH_EDITOR
//Update heath logic after editing inside editor
void ABaseCharacter::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	this->isDead = false;
	this->Health = 100;

	Super::PostEditChangeProperty(PropertyChangedEvent);

	this->CalculateDead();
}
#endif