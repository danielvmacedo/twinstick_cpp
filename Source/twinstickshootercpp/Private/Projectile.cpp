// Fill out your copyright notice in the Description page of Project Settings.

#include "twinstickshootercpp.h"
#include "Projectile.h"
#include "iDamageable.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent * DefaultSceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));
	this->SetRootComponent(DefaultSceneRoot);

	this->ProjectileCollision = CreateDefaultSubobject<USphereComponent>(TEXT("ProjectileCollision"));
	this->ProjectileCollision->InitSphereRadius(10.0f);
	this->ProjectileCollision->AttachTo(GetRootComponent());
	this->ProjectileCollision->SetCollisionProfileName(TEXT("OverlapOnlyPawn"));
	this->ProjectileCollision->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::OnBeginOverlap);

	this->LaserMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("LaserMesh"));
	this->LaserMesh->AttachTo(GetRootComponent());

	static ConstructorHelpers::FObjectFinder<UStaticMesh> LaserMeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	this->LaserMesh->SetStaticMesh(LaserMeshAsset.Object);
	this->LaserMesh->SetRelativeLocation(FVector(-18.0f, 0.0f, 0.0f));
	this->LaserMesh->SetRelativeScale3D(FVector(0.65f, 0.07f, 0.07f));

	static ConstructorHelpers::FObjectFinder<UMaterial> LaserMeshMaterialAsset(TEXT("Material'/Game/Materials/ProjectileMat.ProjectileMat'"));
	this->LaserMesh->SetMaterial(0, LaserMeshMaterialAsset.Object);
	this->LaserMesh->SetCollisionProfileName(TEXT("NoCollision"));

	this->ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	this->ProjectileMovement->SetUpdatedComponent(GetRootComponent());
	this->ProjectileMovement->InitialSpeed = 1200.0f;
	this->ProjectileMovement->ProjectileGravityScale = 0.0f;

	this->Damage = -20;
	this->InitialLifeSpan = 1.5f;
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}


void AProjectile::OnBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	IiDamageable * damageable = Cast<IiDamageable>(OtherActor);
	if (damageable && !OtherActor->Tags.Contains(TEXT("Friendly"))) {
		IiDamageable::Execute_AffectHealth(damageable->_getUObject(), this->Damage); //This way works for c++ and blueprints
	}

	Destroy();
}