// Fill out your copyright notice in the Description page of Project Settings.

#include "twinstickshootercpp.h"
#include "HeroCharacter.h"
#include "TwinStickHUD.h"

AHeroCharacter::AHeroCharacter() :ABaseCharacter()
{
	//Set the hero mesh
	static ConstructorHelpers::FObjectFinder<USkeletalMesh> HeroMeshAsset(TEXT("SkeletalMesh'/Game/AnimationAssets/UE4_Mannequin/Mesh/SK_Mannequin.SK_Mannequin'"));
	this->GetMesh()->SetSkeletalMesh(HeroMeshAsset.Object);

	static ConstructorHelpers::FObjectFinder<UAnimBlueprintGeneratedClass> HeroAnimBPAsset(TEXT("AnimBlueprintGeneratedClass'/Game/AnimationAssets/CharacterAnimBP.CharacterAnimBP_C'"));
	this->GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	this->GetMesh()->AnimBlueprintGeneratedClass = HeroAnimBPAsset.Object;

	/*
	static ConstructorHelpers::FObjectFinder<UAnimBlueprint> HeroAnimBPAsset(TEXT("AnimBlueprint'/Game/AnimationAssets/CharacterAnimBP.CharacterAnimBP'"));
	this->GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	this->GetMesh()->AnimBlueprintGeneratedClass = HeroAnimBPAsset.Object->GetAnimBlueprintGeneratedClass();
	*/

	//Set hero position
	this->GetMesh()->SetRelativeLocation(FVector(0.0f, 0.0f, -90.0f));
	this->GetMesh()->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	//Camera boom
	this->SelfieStick = CreateDefaultSubobject<USpringArmComponent>(TEXT("SelfieStick"));
	this->SelfieStick->AttachTo(this->GetCapsuleComponent());
	this->SelfieStick->SetRelativeRotation(FRotator(-70.0f, 0.0f, 0.0f));
	this->SelfieStick->bInheritRoll = false;
	this->SelfieStick->bInheritPitch = false;
	this->SelfieStick->bInheritYaw = false;
	this->SelfieStick->bDoCollisionTest = false;
	this->SelfieStick->TargetArmLength = 530.0f;

	//Hero Camera
	this->HeroCam = CreateDefaultSubobject<UCameraComponent>(TEXT("HeroCam"));
	this->HeroCam->AttachTo(this->SelfieStick);

	//Gun Temp
	this->GunTemp = CreateDefaultSubobject<UArrowComponent>(TEXT("GunTemp"));
	this->GunTemp->AttachTo(GetRootComponent());
	this->GunTemp->SetRelativeLocation(FVector(-6.0f, 33.0f, 11.0f));


	//Tag
	this->Tags.Add(TEXT("Friendly"));
}

void AHeroCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	check(InputComponent);

	InputComponent->BindAxis("MoveUp", this, &AHeroCharacter::MoveUp);
	InputComponent->BindAxis("MoveRight", this, &AHeroCharacter::MoveRight);

	InputComponent->BindAxis("LookUp");
	InputComponent->BindAxis("LookRight");
}

void AHeroCharacter::Tick(float DeltaSeconds)
{
	float XValue = this->InputComponent->GetAxisValue("LookUp");
	float YValue = this->InputComponent->GetAxisValue("LookRight");
	FVector direction(XValue, YValue, 0.0f);

	if (direction.Size() > 0.25f) {
		APlayerController * pController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		pController->SetControlRotation(direction.Rotation());

		if (this->Weapon) {
			this->Weapon->PullTrigger();
		}
		//UE_LOG(LogTemp, Warning, TEXT("Rotating"));
	}
	else {
		if (this->Weapon) {
			this->Weapon->ReleaseTrigger();
		}
	}
}

void AHeroCharacter::BeginPlay()
{
	UWorld * World = GetWorld();

	if (World) {
		this->GameMode = Cast<AtwinstickshootercppGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		this->GameMode->PlayerSpawnTransform = this->GetTransform();

		FTransform SpawnPoint = this->GunTemp->GetComponentTransform();
		this->Weapon = World->SpawnActor<AWeapon>(SpawnPoint.GetLocation(), SpawnPoint.GetRotation().Rotator());
		//this->Weapon->AttachRootComponentTo(this->GunTemp, NAME_None, EAttachLocation::SnapToTarget);
		this->Weapon->AttachRootComponentTo(this->GetMesh(), TEXT("GunSocket"), EAttachLocation::SnapToTarget);

		UClass * HUDAssetClass = StaticLoadClass(UObject::StaticClass(), nullptr, TEXT("WidgetBlueprint'/Game/Blueprints/BP_TwinStickHUD.BP_TwinStickHUD_C'"));
		UUserWidget * HUD = CreateWidget<UUserWidget>(GetWorld(), HUDAssetClass);
		HUD->AddToViewport();

		/*
		//UObject * HUDAsset = StaticLoadObject(UObject::StaticClass(), nullptr, TEXT("WidgetBlueprint'/Game/Blueprints/BP_TwinStickHUD.BP_TwinStickHUD'"));
		UBlueprint * HUDBlueprint = Cast<UBlueprint>(HUDAsset);
		UTwinStickHUD* HUD = CreateWidget<UTwinStickHUD>(GetWorld(), HUDBlueprint->GeneratedClass);
		HUD->AddToViewport();
		*/
	}
}

void AHeroCharacter::MoveUp(float Value)
{
	this->AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
}

void AHeroCharacter::MoveRight(float Value)
{
	this->AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
}

void AHeroCharacter::AffectHealth_Implementation(float Delta)
{
	this->CalculateHealth(Delta);
	if (this->isDead) {
		this->DisableInput(UGameplayStatics::GetPlayerController(GetWorld(), 0));
		
		if (!GetWorldTimerManager().IsTimerActive(DeadAnimationTimerHandler)) {
			GetWorldTimerManager().SetTimer(DeadAnimationTimerHandler, this, &AHeroCharacter::DestroyHero, 3.0f, false);
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("HERO TAKING DAMAGE %f"), this->Health);
}

void AHeroCharacter::DestroyHero()
{
	this->GameMode->RespawnPlayer();
	if (this->Weapon) {
		this->Weapon->Destroy();
	}
	Destroy();
}