// Fill out your copyright notice in the Description page of Project Settings.

#include "twinstickshootercpp.h"
#include "twinstickshootercppGameMode.h"
#include "HeroCharacter.h"
#include "EnemyCharacter.h"

AtwinstickshootercppGameMode::AtwinstickshootercppGameMode()
{
	DefaultPawnClass = AHeroCharacter::StaticClass();

	this->EnemiesPerSecond = 2;
}

void AtwinstickshootercppGameMode::RespawnPlayer()
{
	UWorld * World = GetWorld();

	if (World) {
		TArray<AActor *> AllOutActors;
		UGameplayStatics::GetAllActorsOfClass(World, AEnemyCharacter::StaticClass(), AllOutActors);

		for (AActor * EachActor:AllOutActors) {
			EachActor->Destroy();
		}

		AHeroCharacter * NewPlayerCharacter = World->SpawnActor<AHeroCharacter>(this->PlayerSpawnTransform.GetLocation(), this->PlayerSpawnTransform.GetRotation().Rotator());
		APlayerController * PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		PlayerController->Possess(NewPlayerCharacter);
	}
}


void AtwinstickshootercppGameMode::BeginPlay()
{
	TArray<AActor*> AllOutActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemySpawner::StaticClass(), AllOutActors);
	if (AllOutActors.Num() > 0) {
		this->Spawner = Cast<AEnemySpawner>(AllOutActors[0]);
	}

	if (this->Spawner) {
		FTimerHandle SpawnTimerHandler;
		GetWorldTimerManager().SetTimer(SpawnTimerHandler, this->Spawner, &AEnemySpawner::SpawnEnemy, 1.0f / this->EnemiesPerSecond, true);
	}
	
}

void AtwinstickshootercppGameMode::IncrementScore(int32 DeltaScore)
{
	this->Score += DeltaScore;
}