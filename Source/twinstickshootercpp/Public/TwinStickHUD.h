// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "TwinStickHUD.generated.h"

/**
 * 
 */
UCLASS()
class TWINSTICKSHOOTERCPP_API UTwinStickHUD : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "TwinStick HUD")
	float HealthBar();

	UFUNCTION(BlueprintCallable, Category = "TwinStick HUD")
	int32 Score();
};
