// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "EnemyAI.generated.h"

/**
 * 
 */
UCLASS()
class TWINSTICKSHOOTERCPP_API AEnemyAI : public AAIController
{
	GENERATED_BODY()
	
	
public:

	virtual void BeginPlay() override;

	void TrackPlayer();


	
};
