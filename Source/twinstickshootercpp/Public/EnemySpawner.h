// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "EnemySpawner.generated.h"

UCLASS()
class TWINSTICKSHOOTERCPP_API AEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Enemy Spawner")
	UBoxComponent * SpawnVolume;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Enemy Spawner")
	int32 MaxEnemies;

	// Sets default values for this actor's properties
	AEnemySpawner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = "Enemy Spawner")
	void SpawnEnemy();
	
};
