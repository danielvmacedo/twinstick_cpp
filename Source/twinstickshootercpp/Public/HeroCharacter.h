// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseCharacter.h"
#include "Weapon.h"
#include "iDamageable.h"
#include "twinstickshootercppGameMode.h"
#include "HeroCharacter.generated.h"

/**
 * 
 */
UCLASS()
class TWINSTICKSHOOTERCPP_API AHeroCharacter : public ABaseCharacter, public IiDamageable
{
	GENERATED_BODY()

	AHeroCharacter();

public:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Hero Character")
	USpringArmComponent * SelfieStick;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Hero Character")
	UCameraComponent * HeroCam;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Hero Character")
	UArrowComponent * GunTemp;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Hero Character")
	AWeapon * Weapon;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Hero Character")
	AtwinstickshootercppGameMode * GameMode;

	virtual void BeginPlay();

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	virtual void Tick(float DeltaSeconds) override;
	
	void MoveUp(float Value);

	void MoveRight(float Value);

	void AffectHealth_Implementation(float Delta) override;

private:
	FTimerHandle DeadAnimationTimerHandler;
	void DestroyHero();
};
