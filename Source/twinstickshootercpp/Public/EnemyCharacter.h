// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseCharacter.h"
#include "iDamageable.h"
#include "HeroCharacter.h"
#include "EnemyCharacter.generated.h"

/**
 * 
 */
UCLASS()
class TWINSTICKSHOOTERCPP_API AEnemyCharacter : public ABaseCharacter, public IiDamageable
{
	GENERATED_BODY()


		AEnemyCharacter();
	
private:
	UMaterialInstanceDynamic * DynaMat;
	FTimerHandle DamageTimerHandler;

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Enemy")
	FLinearColor EnemyColor;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Enemy")
	UBoxComponent * DamageVolume;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Enemy")
	AHeroCharacter * Hero;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Enemy")
	int32 Damage;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Enemy")
	int32 AttacksPerSecond;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Enemy")
	int32 Score;

	virtual void OnConstruction(const FTransform& Transform) override;

	void AffectHealth_Implementation(float Delta) override;

	UFUNCTION()
	void OnBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnEndOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable, Category = "Enemy")
	void DamageTheHero();

private:
	FTimerHandle DeadAnimationTimerHandler;
	void DestroyEnemy();

};
