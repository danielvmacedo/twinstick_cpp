// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class TWINSTICKSHOOTERCPP_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Projectile")
	USphereComponent * ProjectileCollision;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Projectile")
	UStaticMeshComponent * LaserMesh;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Projectile")
	UProjectileMovementComponent * ProjectileMovement;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Projectile")
	int32 Damage;

	// Sets default values for this actor's properties
	AProjectile();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION()
	void OnBeginOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	
};
