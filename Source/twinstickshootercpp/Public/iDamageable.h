// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "iDamageable.generated.h"

/**
 * 
 */
UINTERFACE()
class TWINSTICKSHOOTERCPP_API UiDamageable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class IiDamageable
{
	GENERATED_IINTERFACE_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "iDamageable")
	void AffectHealth(float Delta); //AffectHealth_Implementation in C++

	//virtual void AffectHealth(float Delta) = 0; //pure virtual only c++
};
