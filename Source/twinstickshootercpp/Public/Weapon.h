// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

UCLASS()
class TWINSTICKSHOOTERCPP_API AWeapon : public AActor
{
	GENERATED_BODY()
	
private:
	bool DoOnceFlag;
	FTimerHandle TriggerTimerHandler;

public:	

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Gun")
	int32 RoundsPerSecond;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Gun")
	USkeletalMeshComponent * GunMesh;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Gun")
	UArrowComponent * ProjectileSpawnPoint;

	// Sets default values for this actor's properties
	AWeapon();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = "Gun")
	void PullTrigger();

	UFUNCTION(BlueprintCallable, Category = "Gun")
	void ReleaseTrigger();

	UFUNCTION(BlueprintCallable, Category = "Gun")
	void Fire();
	
};
