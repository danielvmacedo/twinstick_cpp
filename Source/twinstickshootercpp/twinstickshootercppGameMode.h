// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "EnemySpawner.h"
#include "twinstickshootercppGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TWINSTICKSHOOTERCPP_API AtwinstickshootercppGameMode : public AGameMode
{
	GENERATED_BODY()

	AtwinstickshootercppGameMode();
	
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "TwinShooter Game Mode")
	FTransform PlayerSpawnTransform;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "TwinShooter Game Mode")
	AEnemySpawner * Spawner;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "TwinShooter Game Mode")
	int32 EnemiesPerSecond;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "TwinShooter Game Mode")
	int32 Score;

	UFUNCTION(BlueprintCallable, Category = "TwinShooter Game Mode")
	void RespawnPlayer();

	UFUNCTION(BlueprintCallable, Category = "TwinShooter Game Mode")
	void IncrementScore(int32 DeltaScore);

	virtual void BeginPlay();
	
};
